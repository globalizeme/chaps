<div>
	<h2><?php the_title(); ?></h2>
	<p ><?php the_date(); ?> by <a href="#"><?php the_author(); ?></a></p>
	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	} ?>
	<?php the_content(); ?>

</div>